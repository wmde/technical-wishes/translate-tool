# Translate Tool

Tampermonkey script to automatically copy translations tags over to DeepL

![](https://gitlab.com/wmde/technical-wishes/translate-tool/-/raw/main/screencast.gif)
