// ==UserScript==
// @name         Wikipedia Deepl Translator
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Tampermonkey script to automatically copy translations tags over to deepl
// @author       You
// @match        https://www.mediawiki.org/w/index.php?title=Special:Translate&group=page-Help%3AExtension%3AKartographer&action=page&filter=%21translated&language=de
// @match        https://www.deepl.com/translator
// @icon         https://www.google.com/s2/favicons?sz=64&domain=mediawiki.org
// @grant        none
// @require      http://code.jquery.com/jquery-latest.js
// ==/UserScript==
// TODO: make translation page a parameter
// TODO: instead of auto click add a button/link to wikipedia that links to deepl with the text
// TODO: add copy back button to deepl

(function() {
    'use strict';

    // wait for it ...
    // https://newbedev.com/javascript-jquery-wait-until-element-exists-code-example
    var checkExist = setInterval(function() {

        if ($('.untranslated').length) {
            clearInterval(checkExist);

            // add auto click to translate page in wikipedia
            $.each( $('.tux-message-item-compact'), function( number, text ) {  autoClick(this);  });
            $.each( $('.sourcemessage'), function( number, text ) {  autoClick(this);  });
        }

    }, 100); // check every 100ms


     function autoClick(element){
        element.onclick = function() {
                // open deepl automatically with text in a new tab
                window.open('https://www.deepl.com/translator#en/de/'+encodeURIComponent(element.innerText), '_blank');

            };
    }


})();
